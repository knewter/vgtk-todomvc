#![recursion_limit = "512"]

mod config;
mod static_resources;

use config::{APP_ID, GETTEXT_PACKAGE, LOCALEDIR, NAME_SUFFIX, VERSION};
use gettextrs::*;
use log::{debug, info};
use vgtk::ext::*;
use vgtk::lib::gdk_pixbuf::Pixbuf;
use vgtk::lib::gio::{ActionExt, ApplicationFlags, SimpleAction};
use vgtk::lib::gtk::*;
use vgtk::lib::gdk;
use vgtk::lib::gtk;
use vgtk::{gtk, gtk_if, run, Callback, Component, UpdateAction, VNode};

pub struct AboutDialog {
    dog: Pixbuf,
}

impl Default for AboutDialog {
    fn default() -> Self {
        let dog =
            Pixbuf::from_resource_at_scale("/com/dbadbadba/vgtk-todomvc/dog.png", 300, 300, true)
                .unwrap();
        AboutDialog { dog }
    }
}

impl Component for AboutDialog {
    type Message = ();
    type Properties = ();

    fn view(&self) -> VNode<Self> {
        gtk! {
            <Dialog::with_buttons(
                Some("About The Todo List"),
                None as Option<&Window>,
                DialogFlags::MODAL,
                &[("Ok", ResponseType::Ok)]
            )>
                <Box spacing=10 orientation=Orientation::Vertical>
                    <Image pixbuf=Some(self.dog.clone()) />
                    <Label markup="<big><b>A Very Nice Todo List</b></big>" />
                    <Label markup="made with <a href=\"http://vgtk.rs/\">vgtk</a> by me" />
                    <Label markup=format!("Version: {}", VERSION) />
                </Box>
            </Dialog>
        }
    }
}

impl AboutDialog {
    #[allow(unused_must_use)]
    fn run() {
        vgtk::run_dialog::<AboutDialog>(vgtk::current_window().as_ref());
    }
}

#[derive(Clone, Debug, Default)]
pub struct Radio {
    pub images: &'static [&'static str],
    pub active: usize,
    on_changed: Callback<usize>,
}

#[derive(Clone, Debug)]
pub enum RadioMessage {
    Changed(usize),
}

impl Component for Radio {
    type Message = RadioMessage;
    type Properties = Self;

    fn create(props: Self) -> Self {
        props
    }

    fn change(&mut self, props: Self) -> UpdateAction<Self> {
        *self = props;
        UpdateAction::Render
    }

    fn view(&self) -> VNode<Self> {
        gtk! {
            <Box widget_name="toggles">
                {
                    self.images.iter().enumerate().map(|(index, image)| gtk! {
                        <ToggleButton image={ *image  }
                                      active={ index == self.active  }
                                      on toggled=|_| RadioMessage::Changed(index) />
                    })
                }
            </Box>
        }
    }

    fn update(&mut self, msg: Self::Message) -> UpdateAction<Self> {
        match msg {
            RadioMessage::Changed(index) => {
                self.on_changed.send(index);
                UpdateAction::Render
            }
        }
    }
}

#[derive(Clone, Debug)]
struct Task {
    text: String,
    done: bool,
}

impl Task {
    fn new<S: ToString>(text: S, done: bool) -> Self {
        Self {
            text: text.to_string(),
            done,
        }
    }

    fn render(&self, index: usize) -> VNode<Model> {
        gtk! {
            <ListBoxRow>
                <Box>
                    <CheckButton
                        active=self.done
                        on toggled=|_| Message::Toggle { index }
                    />
                    <Label label=self.label() use_markup=true />
                    <Button
                        Box::pack_type=PackType::End
                        relief=ReliefStyle::None
                        image="edit-delete-symbolic"
                        on clicked=|_| Message::Delete { index }
                        on realize=|w| {
                            let context = w.get_style_context();
                            context.add_class("delete-button");

                            Message::None
                        }
                    />
                </Box>
            </ListBoxRow>
        }
    }

    fn label(&self) -> String {
        if self.done {
            format!(
                "<span strikethrough=\"true\" alpha=\"50%\">{}</span>",
                self.text
            )
        } else {
            self.text.clone()
        }
    }
}

#[derive(Clone, Debug)]
struct Model {
    tasks: Vec<Task>,
    filter: usize,
}

impl Model {
    fn filter_task(&self, task: &Task) -> bool {
        match self.filter {
            // "All"
            0 => true,
            // "Active"
            1 => !task.done,
            // "Completed"
            2 => task.done,
            // index out of bounds
            _ => unreachable!(),
        }
    }

    fn items_left(&self) -> String {
        let left = self.tasks.iter().filter(|task| !task.done).count();
        let plural = if left == 1 { "item" } else { "items" };
        format!("{} {} left", left, plural)
    }

    fn count_completed(&self) -> usize {
        self.tasks.iter().filter(|task| task.done).count()
    }
}

impl Default for Model {
    fn default() -> Self {
        Self {
            tasks: vec![
                Task::new("Call Joe", true),
                Task::new("Call Mike", true),
                Task::new("Call Robert", false),
                Task::new("Get Robert to fix the bug", false),
            ],
            filter: 0,
        }
    }
}

#[derive(Clone, Debug)]
enum Message {
    Exit,
    About,
    None,
    Toggle { index: usize },
    Add { task: String },
    Delete { index: usize },
    Filter { filter: usize },
    Cleanup,
}

impl Component for Model {
    type Message = Message;
    type Properties = ();

    fn update(&mut self, msg: Self::Message) -> UpdateAction<Self> {
        match msg {
            Message::None => UpdateAction::None,
            Message::Exit => {
                vgtk::quit();
                UpdateAction::None
            }
            Message::About => {
                AboutDialog::run();
                UpdateAction::None
            }
            Message::Toggle { index } => {
                self.tasks[index].done = !self.tasks[index].done;
                UpdateAction::Render
            }
            Message::Add { task } => {
                self.tasks.push(Task::new(task, false));
                UpdateAction::Render
            }
            Message::Delete { index } => {
                self.tasks.remove(index);
                UpdateAction::Render
            }
            Message::Filter { filter } => {
                self.filter = filter;
                UpdateAction::Render
            }
            Message::Cleanup => {
                self.tasks.retain(|task| !task.done);
                UpdateAction::Render
            }
        }
    }

    fn view(&self) -> VNode<Model> {
        let main_menu = vgtk::menu()
            .section(vgtk::menu().item("About...", "app.about"))
            .section(vgtk::menu().item("Quit", "app.quit"))
            .build();

        gtk! {
            <Application::new_unwrap(Some(APP_ID), ApplicationFlags::empty())>
                <SimpleAction::new("quit", None)
                     Application::accels=["<Ctrl>q"].as_ref()
                     enabled=true
                     on activate=|a, _| Message::Exit />
                <SimpleAction::new("about", None)
                    enabled=true
                    on activate=|_, _| Message::About />

                <Window
                    on destroy=|_| Message::Exit
                    on realize=|win| {
                        debug!(
                            "{} {}",
                            "Component thingie",
                            "win"
                        );
                        Message::None
                    }
                >
                    <HeaderBar title=format!("Erlang: The Todo List{}", NAME_SUFFIX) show_close_button=true>
                        <MenuButton HeaderBar::pack_type=PackType::End
                                    @MenuButtonExt::direction=ArrowType::Down
                                    relief=ReliefStyle::None
                                    image="open-menu-symbolic">
                            <Menu::from_model(&main_menu) />
                        </MenuButton>
                    </HeaderBar>
                    <Box orientation=Orientation::Vertical spacing=10>
                        <Entry
                            widget_name="entry"
                            placeholder_text="What needs to be done?"
                            on activate=|entry| {
                                entry.select_region(0, -1);
                                Message::Add {
                                    task: entry.get_text().to_string()
                                }
                            }
                         />
                        <ScrolledWindow Box::fill=true Box::expand=true>
                            <ListBox
                                widget_name="tasks"
                                selection_mode=SelectionMode::None
                            >
                                {
                                    self.tasks.iter().filter(|task| self.filter_task(task))
                                        .enumerate().map(|(index, task)| task.render(index))
                                }
                            </ListBox>
                        </ScrolledWindow>
                        <Box widget_name="items-left">
                            <Label label=self.items_left() />
                            <@Radio Box::center_widget=true active=self.filter
                                    images=["emblem-favorite-symbolic","emblem-important-symbolic","emblem-ok-symbolic"].as_ref()
                                    on changed=|filter| Message::Filter { filter  } />
                            {
                                gtk_if!(self.count_completed() > 0 => {
                                    <Button
                                        image="edit-clear-all-symbolic"
                                        Box::pack_type=PackType::End
                                        on clicked=|_| Message::Cleanup
                                    />
                                })

                            }
                        </Box>
                    </Box>
                </Window>
            </Application>
        }
    }
}

fn main() {
    info!("vgtk-todomvc{} ({})", config::NAME_SUFFIX, config::APP_ID);
    info!("Version: {} ({})", config::VERSION, config::PROFILE);
    info!("Datadir: {}", config::PKGDATADIR);

    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    pretty_env_logger::init();
    static_resources::init().expect("Failed to initialize the resource file.");
    gtk::init().expect("Error initializing gtk.");

    let provider = CssProvider::new();
    CssProvider::load_from_resource(&provider, "/com/dbadbadba/vgtk-todomvc/style.css");
    StyleContext::add_provider_for_screen(
        &gdk::Screen::get_default().expect("Error initializing gtk css provider"),
        &provider,
        STYLE_PROVIDER_PRIORITY_APPLICATION
    );

    std::process::exit(run::<Model>());
}
