# cargo-template-vgtk

Cargo template for `vgtk` applications.

## Develop

```sh
cargo run
```

...hack

```sh
cargo run
```

(yeah something more integrated and hot-swappy would be nice but I'm not complainig)

## Build

```sh
meson compile -C _builddir
ninja -C _builddir
# Or build the distribution (flatpak) with
ninja -C _builddir dist
```

(You don't need to run `meson` more than once generally, after that you can just run `ninja`)

TODO: Makefile

You can build the flatpak package with:

```sh
flatpak-builder --install repo build-aux/com.dbadbadba.vgtk-todomvc.json --force-clean --user -y
```

## Install

```sh
meson install -C _builddir
```
